# Get a list of all files in the current directory
$files = Get-ChildItem -File

# Loop through each file
foreach ($file in $files) {
    # Check if the file has an extension other than .json
    if ($file.Extension -ne '.json') {
        # Get the filename without the extension
        $nameWithoutExtension = [System.IO.Path]::GetFileNameWithoutExtension($file.FullName)

        # Compare the length of the filename without the extension to the maximum length
        if ($nameWithoutExtension.Length -gt 44) {
            # Get the corresponding JSON file, if it exists
            $json_file = Get-ChildItem -Path $file.DirectoryName -Filter "$($nameWithoutExtension.Substring(0, 44))*.json" | Where-Object { $_.Name -like "$($nameWithoutExtension.Substring(0, 44))*.json" }

            # if there is a corresponding JSON file, use its name and extension
            if ($json_file) {
                $newName = $json_file.Name -replace '\.json$',$file.Extension
                Write-Output $newName
                Rename-Item $file.FullName $newName
            }
            # if there is no corresponding JSON file, truncate the name and keep the original extension
            else {
                $newName = $nameWithoutExtension+ $file.Extension
                # Write-Output $newName
                # Rename-Item $file.FullName $newName
            }
        }
    }
}
