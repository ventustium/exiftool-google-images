$directory = "" 
exiftool -r -d %s -tagsfromfile "%d/%F.json" "-FileCreateDate<PhotoTakenTimeTimestamp" -ext "*" -overwrite_original --ext json $directory
exiftool -r -d %s -tagsfromfile "%d/%F.json" "-FileModifyDate<PhotoTakenTimeTimestamp" -ext "*" -overwrite_original -progress --ext json $directory
exiftool -r -d %s -TagsFromFile "%d/%f.json" "-FileModifyDate<PhotoTakenTimeTimestamp" "-FileName<${filename;s/\.[^.]+$/.mp4/}" -ext "*" -overwrite_original $directory
